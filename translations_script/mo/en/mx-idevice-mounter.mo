��          �            h     i     o     u  H   �     �  b   �  l   =  6   �     �     �     �       �   	     �  @  �     �     �     �  H   �     G  b   L  l   �  6        S     f     l     s  �   {                                                             	                 
       About Close Copyright (c) MX Linux GUI program for mounting & unmounting <br> iPhones and Ipads in MX Linux Help If the information below describes your Apple device press the "Mount" button to access its files. If you get a  "Trust This Computer?"  popup on your device when you plug it in, answer "Trust", then Rescan. If your device is not found, make sure it is unlocked. MX iDevice Mounter Mount Rescan Unmount Use the Unmount button before you remove your device.  If you have already unplugged it, plug your device back in, Rescan, and Unmount. Version: Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-12-02 11:04-0500
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
 About Close Copyright (c) MX Linux GUI program for mounting & unmounting <br> iPhones and Ipads in MX Linux Help If the information below describes your Apple device press the "Mount" button to access its files. If you get a  "Trust This Computer?"  popup on your device when you plug it in, answer "Trust", then Rescan. If your device is not found, make sure it is unlocked. MX iDevice Mounter Mount Rescan Unmount Use the Unmount button before you remove your device.  If you have already unplugged it, plug your device back in, Rescan, and Unmount. Version: 